package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	//public String Firstrecord;
	
	public MergeLeadsPage clickFromLeadImage() {
		WebElement eleFromLeadImage = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(eleFromLeadImage);
		return this;
			}
	
	public MergeLeadsPage clickToLeadImage() {
		WebElement eleToLeadImage = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(eleToLeadImage);
		return this;
			}
	
	public MergeLeadsPage switchWindow(int index) {
		switchToWindow(index);
		return this;
			}
	
		
		public MergeLeadsPage typePhonenumber(String data) {
			WebElement elePhonenumber = locateElement("name", "phoneNumber");
			type(elePhonenumber,data);
			return this;
		}
		
		public MergeLeadsPage typeLeadID() {
			WebElement eleLeadID = locateElement("name", "id");
			type(eleLeadID,Firstrecord);
			return this;
		}
		public MergeLeadsPage verifyErrorMsg() {
			WebElement eleVerify = locateElement("class", "x-paging-info");
			verifyExactText(eleVerify, errorMsg);
			return this;
		}
		
		
		public MergeLeadsPage clickEmail() {
			WebElement eleEmail = locateElement("xpath", "//span[text()='Email']");
			click(eleEmail);
			return this;
		}
		
		public MergeLeadsPage typeEmailAddress(String email) {
			WebElement eleEmailAddress = locateElement("name", "emailAddress");
			type(eleEmailAddress,email);
			return this;
		}
		public MergeLeadsPage clickFindLeads() throws InterruptedException {
			WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
			click(eleFindLeads);
			Thread.sleep(2000);
			/*WebElement message = locateElement("xpath","//div[@class='x-paging-info']");
			String eleVerify = message.getText();*/
			return this;
		}	
		public ViewLeadPage clickFirstLead() {
			WebElement eleFirstLead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String Firstrecord = eleFirstLead.getText();
			click(eleFirstLead);
			return new ViewLeadPage() ;
		}	
	
}









